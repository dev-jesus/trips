import { Router } from "express";
import { startMiddle, } from '../middlewares/create-start.middleware';
import { endMiddle } from '../middlewares/create-end.middleware';
import TripsController from "../classes/trips";


const tripController = new TripsController();
const tripsRouters = Router();



tripsRouters.get('/', tripController.tripsGetController);


tripsRouters.post('/', [startMiddle, endMiddle], tripController.tripsPostController);

export default tripsRouters;