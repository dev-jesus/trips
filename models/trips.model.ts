


import { Schema ,model} from 'mongoose';

const tripShema = new Schema({
    start: {
        time: {
            type: Number
        },
        lat: {
            type: Number
        },
        lon: {
            type: Number
        }, address: {
            type: String
        }
    },
    end: {
        time: {
            type: Number
        },
        lat: {
            type: Number
        },
        lon: {
            type: Number
        }, address: {
            type: String
        }
    },
    distance: {
        type: Number
    },
    duration: {
        type: Number
    },
    overspeedsCount: {
        type: Number
    },
    boundingBox :{
        type: Array
    }
})

export const Trips = model('trips',tripShema);