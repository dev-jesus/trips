import { NextFunction, Request, Response } from "express";
import Address from '../classes/address';
import { IReadings } from '../interfaces/readings.inteface';

export const startMiddle = async (req: Request, res: Response, next: NextFunction) => {

    let times: any = Array();
    let startIndex: number;
    let timeMin: number;
    let readings: IReadings[] = req.body.readings;
    let nombreAddress:string="";

    if (readings.length < 5) { //primera validacion
        return res.json({
            ok: false,
            message: 'Para construir el viaje deben haber por lo menos 5 readings'
        });

    }

    for (let index = 0; index < readings.length; index++) {//SeguntaValidacion
        const element = readings[index];

        if (!element.time) {
            return res.json({
                ok: false,
                message: 'Todos los readings deben tener la propiedad time',
                index
            });
        }

        times.push(element.time);
    }

    timeMin = Math.min(...times);
    startIndex = times.indexOf(timeMin);

    console.log(timeMin);

    const address = new Address();

    await address.getDirection(readings[startIndex].location.lat, readings[startIndex].location.lon)
        .then((resp: any) => {
            nombreAddress = resp[0].formattedAddress;
        }).catch((err: any) => {
            nombreAddress = 'No fount';
        });

    let startData = {
        time: timeMin,
        lat: readings[startIndex].location.lat,
        lon: readings[startIndex].location.lon,
        address: nombreAddress,
    }

    req.body.times = times;
    req.body.startIndex = startIndex;
    req.body.startData = startData;

    next();


}