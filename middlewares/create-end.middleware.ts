import { NextFunction, Request, Response } from "express";
import Address from '../classes/address';
import { IReadings } from '../interfaces/readings.inteface';

export const endMiddle = async (req: Request, res: Response, next: NextFunction) => {

    let times: any = Array();
    times.push(...req.body.times);
    let endIndex: number;
    let nombreAddress:string="";
    let timeMax: number;
    let readings: IReadings[] = req.body.readings;


    timeMax = Math.max(...times);
    endIndex = times.indexOf(timeMax);

    const address = new Address();

    await address.getDirection(readings[endIndex].location.lat, readings[endIndex].location.lon)
        .then((resp: any) => {
            nombreAddress = resp[0].formattedAddress;
        }).catch((err: any) => {
            nombreAddress = 'No fount';
        });

    let endata = {
        time: timeMax,
        lat: readings[endIndex].location.lat,
        lon: readings[endIndex].location.lon,
        address: nombreAddress,
    }

    req.body.endIndex = endIndex;
    req.body.endata = endata;

    next();

}