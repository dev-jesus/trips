export interface  IReadings {
    time: number;
    speed: number;
    speedLimit: number;
    location: Location;
  }
  
  export interface Location {
    lat: number;
    lon: number;
  }