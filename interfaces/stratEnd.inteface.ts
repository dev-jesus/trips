
import {Document} from 'mongoose';

export interface IstartEnd extends Document {
  _id: string;
  time: number;
  lat: number;
  address: string;
  tipo: number;
}