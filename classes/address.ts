
let nodeGeocoder = require('node-geocoder');

export default class Address {
    public options = {
        provider: 'openstreetmap'
    };  
    public geoCoder;

    constructor(){
        this.geoCoder = nodeGeocoder(this.options);
    }


    getDirection(lat:number,lon:number){
        return this.geoCoder.reverse({ lat, lon });
    }



}