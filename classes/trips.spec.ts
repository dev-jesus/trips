

import { Request, Response } from 'express';
import TripsController from './trips';
import Calculation from './calculations';
import { Trips } from '../models/trips.model';

describe('TripsController', () => {

    let trip = new TripsController();
    let mockResponse:any = { json: () => {} }
    let mockRequest: any = { query: {} };
    let mockRequestpost: any = { body: {} };

    beforeEach(() => {

        mockRequest.query = {
            start_gte: '1',
            start_lte: '',
            distance_gte: '44444',
            limit: '10',
            offset: '1'
        }
        mockRequestpost.body = {
            "readings": [
                {
                    "time": 1642500462000,
                    "speed": 9,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580158,
                        "lon": -70.567227
                    }
                },
                {
                    "time": 1642500466000,
                    "speed": 26,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.58013,
                        "lon": -70.566995
                    }
                },
                {
                    "time": 1642500470000,
                    "speed": 28,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580117,
                        "lon": -70.566633
                    }
                },
                {
                    "time": 1642500474000,
                    "speed": 13,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580078,
                        "lon": -70.566408
                    }
                },
                {
                    "time": 1642500478000,
                    "speed": 18,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580005,
                        "lon": -70.566498
                    }
                },
                {
                    "time": 1642500482000,
                    "speed": 32,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.58002,
                        "lon": -70.566837
                    }
                },
                {
                    "time": 1642500486000,
                    "speed": 38,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580038,
                        "lon": -70.567265
                    }
                },
                {
                    "time": 1642500490000,
                    "speed": 38,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580043,
                        "lon": -70.56773
                    }
                },
                {
                    "time": 1642500494000,
                    "speed": 35,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580048,
                        "lon": -70.56817
                    }
                },
                {
                    "time": 1642500498000,
                    "speed": 20,
                    "speedLimit": 38,
                    "location": {
                        "lat": -33.580053,
                        "lon": -70.568502
                    }
                }
            ],
            times: [
                1642500462000,
                1642500466000,
                1642500470000,
                1642500474000,
                1642500478000,
                1642500482000,
                1642500486000,
                1642500490000,
                1642500494000,
                1642500498000
            ],
            endIndex:9,
            startIndex:0
        }
        jest.spyOn(Trips, 'create').mockImplementationOnce(() => Promise.resolve({} as any));

    })


    test('TripsController - tripsGetController', () => {

        expect(trip.tripsGetController(mockRequest as Request, mockResponse as Response)).toBeDefined();
    });

    test('TripsController - tripsPostController', () => {
        trip.tripsPostController(mockRequestpost as Request, mockResponse as Response);
        expect(trip).toBeDefined();
    });
})