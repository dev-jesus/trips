

import Calculation from './calculations';
var BoundingBox = require('boundingbox');

describe('Calculation', () => {

    let calc = new Calculation();
    let mockBox:any;
    beforeEach(() => {
        BoundingBox = jest.fn();
        mockBox = { "maxlat": -33.580053, "maxlon": -70.568502, "minlat": -33.580158, "minlon": -70.567227 }

    })


    test('Calculation -getDistance', () => {
        expect(calc.getDistance({ lat: -33.580158, lon: -70.567227 }, { lat: -33.580053, lon: -70.568502 })).toBeDefined();
    });

    test('Calculation - getBoundingBox', () => {
        expect(calc.getBoundingBox(-33.580158, -70.567227, -33.580053, -70.568502)).toEqual(mockBox);
    });
})