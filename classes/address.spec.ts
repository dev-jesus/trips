

import Address from './address';
let nodeGeocoder = require('node-geocoder');

describe('class Address', () => {

    let address=new Address();
    
    beforeEach(() => {
        nodeGeocoder= jest.fn();

    })


    test('Address - getDirection ', () => {
        
        expect(address.getDirection(10,22)).toBeDefined();
    });
})