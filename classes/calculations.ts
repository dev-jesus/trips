import { Location, IReadings } from '../interfaces/readings.inteface';
var BoundingBox = require('boundingbox');

export default class Calculation {

    public distance:number=0;
    public ope1:number=0;
    public ope2:number=0;

    constructor(){

    }

    getDistance(pointA: Location, pointB: Location): number {
        
        var radius = 6371; // km     
    
        const deltaLatitude = (pointB.lat - pointA.lat) * Math.PI / 180;
        const deltaLongitude = (pointB.lon - pointA.lon) * Math.PI / 180;
    
        const halfChordLength = Math.cos(
            pointA.lat * Math.PI / 180) * Math.cos(pointB.lon * Math.PI / 180) 
            * Math.sin(deltaLongitude/2) * Math.sin(deltaLongitude/2)
            + Math.sin(deltaLatitude/2) * Math.sin(deltaLatitude/2);
    
        const angularDistance = 2 * Math.atan2(Math.sqrt(halfChordLength), Math.sqrt(1 - halfChordLength));
    
        return radius * angularDistance;
    }

    getBoundingBox( minlat: number, minlon: number, maxlat: number, maxlon: number ){
       
       let boxs=new BoundingBox({minlat,minlon,maxlat,maxlon});

       return boxs || [];
       
    }
    

    
}