import { Request, Response } from "express";
import { Trips } from '../models/trips.model';
import Calculation from './calculations';
import { IReadings } from '../interfaces/readings.inteface';

export default class TripsController {

    constructor() { }

    tripsPostController(req: Request, res: Response) {


        let duration: number = 0;
        let readings: IReadings[] = req.body.readings;
        let distance: number = 0;
        let boundingBox: any;
        let overspeedsCount: number = 0;
        const calc = new Calculation();
        
        duration = readings[req.body.endIndex].time - readings[req.body.startIndex].time;
        distance = calc.getDistance(readings[req.body.startIndex].location, readings[req.body.endIndex].location);
        boundingBox = calc.getBoundingBox(readings[req.body.startIndex].location.lat, readings[req.body.startIndex].location.lon, readings[req.body.endIndex].location.lat, readings[req.body.endIndex].location.lon);
        readings.forEach((reading) => reading.speed > reading.speedLimit ? overspeedsCount++ : overspeedsCount);


        let data = {
            start: req.body.startData,
            end: req.body.endata,
            duration,
            distance,
            overspeedsCount,
            boundingBox
        }

        Trips.create(data).then(tripDB => {
            
            return res.json({
                ok: true,
                tripDB
            })

        }).catch(err => {
            return res.json({
                ok: false,
                err
            })
        })
    }


    async tripsGetController(req: Request, res: Response) {

        let limit = req.query.limit || 10;
        let offset = req.query.offset || 0;
        let sql: any = {};


        if (req.query.start_gte) {
            sql['start.time'] = { $gte: Number(req.query.start_gte) }
        }

        if (req.query.start_lte) {
            sql['start.time'] = { $lte: Number(req.query.start_lte) }
        }

        if (req.query.distance_gte) {
            sql['distance'] = { $gte: Number(req.query.start_lte) }
        }

        let data = await Trips.find().where(sql).sort({ _id: 1 }).limit(Number(limit)).skip(Number(offset));


        return res.json({
            ok: true,
            mensaje: 'Todo bien',
            data
        })
    }


}
