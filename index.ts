import Server from './classes/server';
import tripsRouters from './routes/trips.router';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
require('dotenv').config()
let url:any=process.env.MONGO_URI;

const server = new  Server();

server.start(()=>{
 console.log('Servidor corriendo en puerto '+server.port);
});


//configuracion de cors
server.app.use(cors({origin:true,credentials:true}));

server.app.use(bodyParser.urlencoded({extended:true})); //ver video
server.app.use(bodyParser.json());

//conectarme a la base de datos
mongoose.connect(url,(err)=>{
            if(err) throw err;
            console.log('base de datos online');            
        });

//Rutas Aplicacion
server.app.use('/trips',tripsRouters);